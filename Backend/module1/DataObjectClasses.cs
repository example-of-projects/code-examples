﻿using System.Collections.Generic;

namespace FiasLib
{
    public class DataObjectClasses
    {
    //===========================================================================
        // --------------------- AddressObject  ----------------------
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(ElementName = "Object")]
        public class AddressObject
        {
            [System.Xml.Serialization.XmlAttributeAttribute()]            public string AOGUID;
            [System.Xml.Serialization.XmlAttributeAttribute()]            public string FORMALNAME;
            [System.Xml.Serialization.XmlAttributeAttribute()]            public string REGIONCODE;
            [System.Xml.Serialization.XmlAttributeAttribute()]            public string AUTOCODE;
            [System.Xml.Serialization.XmlAttributeAttribute()]            public string AREACODE;
            [System.Xml.Serialization.XmlAttributeAttribute()]            public string CITYCODE;
            [System.Xml.Serialization.XmlAttributeAttribute()]            public string CTARCODE;
            [System.Xml.Serialization.XmlAttributeAttribute()]            public string PLACECODE;
            [System.Xml.Serialization.XmlAttributeAttribute()]            public string STREETCODE;
            [System.Xml.Serialization.XmlAttributeAttribute()]            public string EXTRCODE;
            [System.Xml.Serialization.XmlAttributeAttribute()]            public string SEXTCODE;
            [System.Xml.Serialization.XmlAttributeAttribute()]            public string OFFNAME;
            [System.Xml.Serialization.XmlAttributeAttribute()]            public string POSTALCODE;
            [System.Xml.Serialization.XmlAttributeAttribute()]            public string IFNSFL;
            [System.Xml.Serialization.XmlAttributeAttribute()]            public string TERRIFNSFL;
            [System.Xml.Serialization.XmlAttributeAttribute()]            public string IFNSUL;
            [System.Xml.Serialization.XmlAttributeAttribute()]            public string TERRIFNSUL;
            [System.Xml.Serialization.XmlAttributeAttribute()]            public string OKATO;
            [System.Xml.Serialization.XmlAttributeAttribute()]            public string OKTMO;
            [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]            public System.DateTime UPDATEDATE;
            [System.Xml.Serialization.XmlAttributeAttribute()]            public string SHORTNAME;
            [System.Xml.Serialization.XmlAttributeAttribute()]            public int AOLEVEL;
            [System.Xml.Serialization.XmlAttributeAttribute()]            public string PARENTGUID;
            [System.Xml.Serialization.XmlAttributeAttribute()]            public string AOID;
            [System.Xml.Serialization.XmlAttributeAttribute()]            public string PREVID;
            [System.Xml.Serialization.XmlAttributeAttribute()]            public string NEXTID;
            [System.Xml.Serialization.XmlAttributeAttribute()]            public string CODE;
            [System.Xml.Serialization.XmlAttributeAttribute()]            public string PLAINCODE;
            [System.Xml.Serialization.XmlAttributeAttribute()]            public int ACTSTATUS;
            [System.Xml.Serialization.XmlAttributeAttribute()]            public int CENTSTATUS;
            [System.Xml.Serialization.XmlAttributeAttribute()]            public int OPERSTATUS;
            [System.Xml.Serialization.XmlAttributeAttribute()]            public int CURRSTATUS;
            [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]            public System.DateTime STARTDATE;
            [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]            public System.DateTime ENDDATE;
            [System.Xml.Serialization.XmlAttributeAttribute()]            public string NORMDOC;
            [System.Xml.Serialization.XmlAttributeAttribute()]            public int LIVESTATUS;
        }

        // ---------------------    Houses      ----------------------
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute("House")]
        public class House
        {
            [System.Xml.Serialization.XmlAttributeAttribute()]            public string POSTALCODE;           // FOR House
            [System.Xml.Serialization.XmlIgnoreAttribute()]               public bool POSTALCODESpecified;  // FOR House
            [System.Xml.Serialization.XmlAttributeAttribute()]            public string IFNSFL;
            [System.Xml.Serialization.XmlIgnoreAttribute()]               public bool IFNSFLSpecified;      // FOR House
            [System.Xml.Serialization.XmlAttributeAttribute()]            public string TERRIFNSFL;
            [System.Xml.Serialization.XmlIgnoreAttribute()]               public bool TERRIFNSFLSpecified;
            [System.Xml.Serialization.XmlAttributeAttribute()]            public string IFNSUL;
            [System.Xml.Serialization.XmlIgnoreAttribute()]              public bool IFNSULSpecified;      // FOR House
            [System.Xml.Serialization.XmlAttributeAttribute()]            public string TERRIFNSUL;
            [System.Xml.Serialization.XmlIgnoreAttribute()]               public bool TERRIFNSULSpecified;
            [System.Xml.Serialization.XmlAttributeAttribute()]            public string OKATO;
            [System.Xml.Serialization.XmlIgnoreAttribute()]               public bool OKATOSpecified;       // FOR House
            [System.Xml.Serialization.XmlAttributeAttribute()]            public string OKTMO;
            [System.Xml.Serialization.XmlIgnoreAttribute()]               public bool OKTMOSpecified;       // FOR House
            [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]            public System.DateTime UPDATEDATE;
            [System.Xml.Serialization.XmlAttributeAttribute()]            public string HOUSENUM;
            [System.Xml.Serialization.XmlAttributeAttribute()]            public int ESTSTATUS;
            [System.Xml.Serialization.XmlAttributeAttribute()]            public string BUILDNUM;
            [System.Xml.Serialization.XmlAttributeAttribute()]            public string STRUCNUM;
            [System.Xml.Serialization.XmlAttributeAttribute()]            public int STRSTATUS;
            [System.Xml.Serialization.XmlAttributeAttribute()]            public string HOUSEID;
            [System.Xml.Serialization.XmlAttributeAttribute()]            public string HOUSEGUID;
            [System.Xml.Serialization.XmlAttributeAttribute()]            public string AOGUID;
            [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]            public System.DateTime STARTDATE;
            [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]            public System.DateTime ENDDATE;
            [System.Xml.Serialization.XmlAttributeAttribute()]            public int STATSTATUS;
            [System.Xml.Serialization.XmlAttributeAttribute()]            public string NORMDOC;            // FOR House
            [System.Xml.Serialization.XmlAttributeAttribute()]            public int COUNTER;
        }
    //===========================================================================
        // ---------------------   FileList     ----------------------

        public class FileParams
        {
            public string SourcePath;
            public List<FileParam> Files;

            public FileParams()
            {
                SourcePath = @"C:\path";
                Files = new List<FileParam>()
                {
                    {new FileParam()
                        { FileMask = "AS_ADDROBJ_*",
                          NodeName = "Object",
                          StoredProc = "some1_update",
                          CustomClass = new Function<AddressObject>()
                        }
                     },
                     {new FileParam()
                        { FileMask = "AS_DEL_ADDROBJ_*",
                          NodeName = "Object",
                          StoredProc = "some1_delete",
                          CustomClass = new Function<AddressObject>()
                        }
                     },
                     {new FileParam()
                        { FileMask = "AS_HOUSE_*",
                          NodeName = "House",
                          StoredProc = "someUpdate",
                          CustomClass = new Function<House>()
                        }
                     },
                     {new FileParam()
                        { FileMask = "AS_DEL_HOUSE_*",
                          NodeName = "House",
                          StoredProc = "someDelete",
                          CustomClass = new Function<House>()
                        }
                     }
                };
            }
        }
        // ---------------------   FileParam    ----------------------
        public class FileParam
        {
            public string FileMask   { get; set; }
            public string StoredProc { get; set; }
            public string NodeName   { get; set; }
            public AFunctions CustomClass;
        }
    //===========================================================================
    }
}
