﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;

namespace FiasLib
{
    public interface IFunctions
    {
        void CheckExists(string FilePath, string FileMask);
        void LoadFile(string NodeName, string StorProc);
        void UpdateDbase(string NodeName, string StorProc, string GUID);
    }

    public abstract class AFunctions : IFunctions
    {
        public string SearchedFilePath;
        public DataTable Ttable;
        public string ConnectionString = @"someString";

        public abstract void CheckExists(string FilePath, string FileMask);
        public abstract void LoadFile(string NodeName, string StorProc);
        public abstract void UpdateDbase(string NodeName, string StorProc, string GUID);
        public abstract void GetTable();
    }

    public class Function<T> : AFunctions where T : class
    {

        public override void CheckExists(string FilePath, string FileMask)
        {
            //exists directory
            if (!Directory.Exists(Path.GetDirectoryName(FilePath)))
            {
                throw new IOException("Directory doesn't exisits!");
            }
            //exists file
            var files = Directory.GetFiles(FilePath, FileMask);
            switch (files.Count())
            {
                case 0: throw new IOException(string.Format("File with mask {0} doesn't exisits",FileMask));
                case 1: SearchedFilePath = files[0];
                    break;
                default: throw new IOException(string.Format("Files with mask {0} more than 1, remove duplicates!", FileMask));
            }
        }
        public override void LoadFile(string NodeName, string StorProc)
        {
            if (string.IsNullOrEmpty(SearchedFilePath))
                throw new IOException("File doesn't exisits");
            GetTable();
            using (XmlReader xmlReader = XmlReader.Create(SearchedFilePath))
            {
                List<T> Objects = new List<T>();
                var serial = new XmlSerializer(typeof(T));
                var i = 0;
                var GUID = Guid.NewGuid().ToString();
                while (xmlReader.ReadToFollowing(NodeName))
                {
                    var obj = (T)serial.Deserialize(xmlReader.ReadSubtree());
                    Objects.Add(obj);
                    if (Objects.Count() > 100000)
                    {
                        i++;
                        Console.WriteLine("Round " + i.ToString());
                        FillTable(Objects);
                        Console.WriteLine("Fight!");
                        UpdateDbase(NodeName,StorProc,GUID);
                        Objects.Clear();

                    }
                }
                Console.WriteLine("Last Round");
                if (Objects.Count() !=0 )
                {
                    FillTable(Objects);
                    Console.WriteLine("Fight!");
                    UpdateDbase(NodeName, StorProc,GUID);
                    Objects.Clear();
                }
                xmlReader.Close();
                Console.WriteLine("Fatality!");
            }
        }
        public override void UpdateDbase(string NodeName, string StorProc, string GUID)
        {
            var param = new List<KeyValuePair<string, object>>()
            {
                { new KeyValuePair<string,object>("@"+NodeName+"Table",Ttable)},
                { new KeyValuePair<string,object>("@GUID", GUID)}
            };
            ExecuteProcedure(ConnectionString, StorProc, CommandType.StoredProcedure, param);
        }
        public override void GetTable()
        {
            Ttable = new DataTable("Table");
            var TypeInfo = typeof(T);
            var Fields = TypeInfo.GetFields();
            foreach (var field in Fields)
            {
                Ttable.Columns.Add(field.Name, field.FieldType);
            }
        }
        // ----------------------------------------------------------------
        private void FillTable(List<T> Objects)
        {
            Ttable.Clear();
            foreach (var item in Objects)
            {
                DataRow row = Ttable.NewRow();
                var info = item.GetType().GetFields();
                foreach (var prop in info)
                {
                    row.SetField(prop.Name, prop.GetValue(item));
                }
                Ttable.Rows.Add(row);
            }
        }
        // ----------------------------------------------------------------
        private void ExecuteProcedure(string ConnectionString, string cmdText, CommandType CommandType, List<KeyValuePair<string, object>> ParamList)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(cmdText, con))
                {
                    con.Open();
                    cmd.CommandType = CommandType;
                    cmd.CommandTimeout = 600;
                    foreach (var param in ParamList)
                    {
                        cmd.Parameters.AddWithValue(param.Key, param.Value);
                    }
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
        }

    }
}
