﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Web;
using some.BL.DAL;
using some.BL.Models;
using Microsoft.AspNet.Identity.Owin;
using System;

namespace some.BL.Providers
{
    public class BlogProvider
    {
        #region DI
        private static ApplicationDbContext _DbContext;
        public static ApplicationDbContext DbContext
        {
            get
            {
                return _DbContext ?? HttpContext.Current.GetOwinContext().Get<ApplicationDbContext>();
            }
            private set
            {
                _DbContext = value;
            }
        }

        #endregion

        public static List<Tag> FetchTags()
        {
            var tags = DbContext.Tag.AsNoTracking().Where(e => e.Enable).ToList();
            return tags;
        }

        #region Post functions
        public static List<Post> FetchAllPosts()
        {
            var posts = DbContext.Post.AsNoTracking()
                                      .Where(e => e.Enable)
                                      .ToList();
            foreach (var post in posts)
            {
                post.Comments = post.Comments.Where(c => c.Enable).ToList();
            }
            return posts;
        }

        public static Post FetchCurrentPost(int id)
        {
            var post = DbContext.Post.AsNoTracking()
                                    .FirstOrDefault(e => e.Enable && e.Id == id);
            post.Comments = post.Comments.Where(c => c.Enable).ToList();
            return post;
        }

        public static void AddOrUpdatePost(Post data)
        {
            var existedPost = DbContext.Post.Find(data.Id);
            if (existedPost != null)
            {
                existedPost.Title = data.Title;
                existedPost.Text = data.Text;
                existedPost.Img = data.Img;
                existedPost.Img_title = data.Img_title;
                existedPost.Img_alt = data.Img_alt;
                existedPost.Img_keywords = data.Img_keywords;
                existedPost.TagId = data.TagId;
            }
            else
            {
                DbContext.Post.Add(data);
            }
            DbContext.SaveChanges();
        }

        public static void MarkRemovedPost(int id)
        {
            var post = DbContext.Post.Find(id);
            if (post != null)
            {
                var comments = FetchCommentsByPostID(id);
                foreach (var item in comments)
                {
                    item.Enable = false;
                };
                post.Enable = false;
                DbContext.SaveChanges();
            }
        }
        #endregion

        #region Comment fuctions
        public static ICollection<Comment> FetchCommentsByPostID(int id)
        {
            var comments = DbContext.Comment.AsNoTracking().Where(e => e.Enable==true && e.postID == id).ToList();
            return comments;
        }

        public static void AddComment(Comment data)
        {
            DbContext.Comment.Add(data);
            DbContext.SaveChanges();

        }

        public static void MarkRemovedComment(Guid guid)
        {
            var comment = DbContext.Comment.Find(guid);
            if (comment != null)
            {
                comment.Enable = false;
                DbContext.SaveChanges();
            }
        }
        #endregion
    }
}
