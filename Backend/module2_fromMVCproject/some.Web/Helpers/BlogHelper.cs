﻿using System;
using System.Collections.Generic;
using System.Linq;
using Some.BL;
using Some.BL.Models;
using Some.BL.Providers;
using Some.Web.Models;

namespace Some.Web.Helpers
{
    public static class BlogHelper
    {
        public static List<TagViewModel> GetAllTags()
        {
            var tags = BlogProvider.FetchTags();
            var tagViewModelList = tags.Select(ToTagViewModel).ToList();
            return tagViewModelList;
        }
        #region Post functions
        public static PostViewModel GetPost(int id)
        {
            var post = BlogProvider.FetchCurrentPost(id);
            var postViewModel = post.With(e => e.ToPostViewModel());
            return postViewModel;
        }

        public static List<PostViewModel> GetAllPosts()
        {
            var posts = BlogProvider.FetchAllPosts();
            var postViewModelList = posts.Select(ToPostViewModel).ToList();
            return postViewModelList;
        }

        public static void AddOrUpdatePost(PostViewModel postViewModel)
        {
            var post = new Post(){
                Id = postViewModel.id,
                Title = postViewModel.title,
                Text = postViewModel.text,
                Img = postViewModel.img,
                Img_title = postViewModel.img_title,
                Img_alt = postViewModel.img_alt,
                Img_keywords = postViewModel.img_keywords,
                TagId = postViewModel.tag,
                DT_Create = DateTime.Now,
                Enable = true
            };
            BlogProvider.AddOrUpdatePost(post);
        }

        public static void RemovePost(int id)
        {
            BlogProvider.MarkRemovedPost(id);
        }

        #endregion


        #region Comment fuctions
        public static List<CommentViewModel> GetCommentsByPostID(int id)
        {
            var comments = BlogProvider.FetchCommentsByPostID(id);
            var commentViewModelList = comments.Select(ToCommentViewModel).ToList();
            return commentViewModelList;
        }
        public static void AddComment(CommentViewModel data)
        {
            var comment = new Comment(){
                id  = Guid.NewGuid(),
                postID = data.postID,
                userID = data.userID,
                text = data.text,
                DT_Create = DateTime.Now.AddHours(3),
                Enable = true
            };
            BlogProvider.AddComment(comment);
        }
        public static void RemoveComment(Guid guid)
        {
            BlogProvider.MarkRemovedComment(guid);
        }

        #endregion

        #region DAL to VM convertions

        private static TagViewModel ToTagViewModel(this Tag model)
        {
            var result = new TagViewModel()
            {
                id = model.Id,
                value = model.Value
            };
            return result;
        }

        private static PostViewModel ToPostViewModel(this Post model)
        {
            var result = new PostViewModel()
            {
                id= model.Id,
                title = model.Title,
                text = model.Text,
                img = model.Img,
                img_title = model.Img_title,
                img_alt = model.Img_alt,
                img_keywords = model.Img_keywords,
                tag = model.TagId,
                dateTime = model.DT_Create
            };
            result.Comments = model.Comments.Safe().Select(ToCommentViewModel).ToList();
            return result;
        }

        private static CommentViewModel ToCommentViewModel(this Comment model)
        {
            var data = new CommentViewModel()
            {
                guid = model.id,
                postID = model.postID,
                userName = (!string.IsNullOrEmpty(model.userID)) ? model.user.Name : "",
                userPhoto = (!string.IsNullOrEmpty(model.userID)) ?model.user.PhotoUrl: "",
                text = model.text,
                dateTime = model.DT_Create
            };
            return data;
        }
        #endregion

    }
}
