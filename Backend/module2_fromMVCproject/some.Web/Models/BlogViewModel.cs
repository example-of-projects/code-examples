﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Some.Web.Models
{
    public class PostViewModel
    {
        public int id               { get; set; }

        [Display(Name = "Заголовок")]
        public string title { get; set; }

        [Display(Name = "Текст")]
        public string text { get; set; }

        [Display(Name = "Тег")]
        public int tag { get; set; }

        [Display(Name = "Изображение")]
        public string img { get; set; }

        public string img_title { get; set; }

        public string img_alt { get; set; }

        public string img_keywords { get; set; }

        [Display(Name = "Дата публикации")]
        public DateTime dateTime    { get; set; }

        public virtual IList<CommentViewModel> Comments { get; set; }
    }

    public class TagViewModel
    {
        public int      id      { get; set; }
        public string   value   { get; set; }
    }

    public class CommentViewModel
    {
        public Guid guid { get; set; }
        public int postID { get; set; }
        public string userID { get; set; }
        public string userName { get; set; }
        public string userPhoto { get;set;}
        public string text { get; set; }
        public DateTime dateTime { get; set; }
    }
}
