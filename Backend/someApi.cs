﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

[WebService(Namespace = "some/api")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]

public class Api : System.Web.Services.WebService {

    public Api () {}


    string strConnString = "someString";

    [WebMethod(Description = "Dictionary")]
    public void GetRef(int project_id, int reference_id)
    {
        var storProcName = "someName";
        var param = new List<KeyValuePair<string, object>>()
            {
                {new KeyValuePair<string,object>("@project_id",project_id)},
                {new KeyValuePair<string,object>("@reference_id",reference_id)}
            };
        var tempColumnQuery = Procedure_FillDataTable(strConnStringOP, storProcName, CommandType.StoredProcedure, param);
        var tResult = ToResultTable(tempColumnQuery);
        string json = JsonConvert.SerializeObject(tResult);
        this.Context.Response.ContentType = "application/json; charset=utf-8";
        this.Context.Response.Write(json);
        this.Context.Response.End();
    }

    //Convert to specific dinamic table
    private static Result ToResultTable(DataTable tempColumnQuery)
    {
        var tResult = new Result();
        var tempColumnInfo = new DataTable();
        tempColumnInfo.Columns.AddRange(new DataColumn[] {  new DataColumn("title"),
                                                            new DataColumn("field"),
                                                            new DataColumn("type"),
                                                            new DataColumn("sortable")
                                                          });
        foreach (DataColumn col in tempColumnQuery.Columns)
        {
            var newName = string.Format("column{0}", tempColumnQuery.Columns.IndexOf(col));
            tempColumnInfo.Rows.Add(new object[] {  col.ColumnName,
                                                    newName,
                                                    col.DataType.Name.ToLower(),
                                                    true
                                                  });
            col.ColumnName = newName;
            foreach (DataRow row_ in tempColumnQuery.Rows)
            {
                if (row_.IsNull(newName))
                    row_[newName] = string.Empty;
            }
        }
        tResult.resultTable = tempColumnQuery;
        tResult.columnInfo = tempColumnInfo;
        return tResult;
    }

    // Обертка для работы с SQL ->  FillDataTable
    private DataTable Procedure_FillDataTable(string ConnectionString, string cmdText, CommandType CommandType, List<KeyValuePair<string, object>> ParamList)
    {
        using (SqlConnection con = new SqlConnection(ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand(cmdText, con))
            {
                con.Open();
                cmd.CommandType = CommandType;
                cmd.CommandTimeout = 300;
                foreach (var param in ParamList)
                {
                    cmd.Parameters.AddWithValue(param.Key, param.Value);
                }
                using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                {
                    var dataTable = new DataTable("DataTable");
                    sda.Fill(dataTable);
                    return dataTable;
                }
            }
        }
    }
}
