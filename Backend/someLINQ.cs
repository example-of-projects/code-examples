﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace PsyReport
{
    public class ReportLINQ
    {
        public string dataFilePath = "";
        public string reportTemplatePath = "";
        public string reportOutPath = "";

        public DataTable CSVfile;
        public List<Patient> DataFile;
        public List<ReportData> FinalReport;
        public List<nulldata> nullData;

        public int reportType;
        public string reportName = "";

        public ReportLINQ(string dataFilePath, string dataFolderPath, string OutPutPath,int repType)
        {
            if (File.Exists(dataFilePath))
            {
                this.dataFilePath = dataFilePath;
            }
            else
            {
                throw new IOException("Таблица не найдена!");
            }
            reportTemplatePath = Path.Combine(dataFolderPath, "template/Template.xls");
            reportOutPath = OutPutPath;
            reportType = repType;
        }
        public ReportLINQ() { }

        public void LoadFile()
        {
            DataFile = new List<Patient>();
            var lines = File.ReadAllLines(dataFilePath, Encoding.GetEncoding(1251))
                .Skip(1)
                .Select(a => a.Split(';'));
            Mask area = new Mask();
            area.MaskList = area.GenerateAreaMask();
            foreach(var line in lines){
                //Убрать лишние пробелы из названия района
                line[8] = line[8]
                    .Trim()
                    .Replace("  ", " ")
                    .Replace(" -", "-")
                    .Replace("- ", "-")
                    .ToLower();
                // Форматируем маску районов
                line[8] = area.Equal(line[8], Mask.TypeMask.Area);
                //повторно (досрочно) => повторно
                line[10] = line[10].Replace("повторно (досрочно)", "повторно");
                // Оставить от кода заболевания 3 символа
                line[12] = line[12].Length > 3 ? line[12].Substring(0, 3) : line[12];
                // Нормальное отображение бюро.
                line[18] = line[18]
                    .Replace("ФКУ \"\"ГБ МСЭ по Республике Татарстан (Татарстан)\"\" Минтруда России - ", "")
                    .Replace("\"","");
                
                DataFile.Add( new Patient(){
                    ProtocolNumber  = line[0],		
                    LastName        = line[1],			
                    FirstName       = line[2],			
                    MiddleName      = line[3],			
                    Gender          = line[4],				
                    Age				= line[5],	
                    AddressFull		= line[6],	
                    AddressCityArea	= line[7],	
                    AddressArea		= line[8],	
                    AddressCity		= line[9],	
                    Exam			= line[10],	
                    GroupDisability	= line[11],	
                    CodeMKB			= line[12],	
                    OrganisationName= line[13],	
                    DateDecision	= line[14],	
                    TermDisability	= line[15],	
                    ObjectivesMSE	= line[16],	
                    DateBirth		= line[17],	
                    Buro	        = line[18]
                });
            }
        }

        public IEnumerable<string> GetBuroList()
        {
            var temp = from patients in DataFile
                       group patients by patients.Buro into g
                       orderby g.Key
                       select g.Key;
            return temp;
        }

        public IEnumerable<GroupingData> GroupMKB(string buro)
        {
            Mask code = new Mask();
            code.MaskList = code.GenerateCodeMask();
            // select only buro
            var query = from patients in DataFile
                        where patients.Buro == buro
                        select new
                        {
                            District = SetDistrict(patients.AddressArea),
                            Area = patients.AddressArea,
                            CityType = patients.AddressCity,
                            Exam = patients.Exam,
                            Code = code.Equal(patients.CodeMKB, Mask.TypeMask.Code)
                        };
            //Select nullData
            nullData = (from p in DataFile
                        where string.IsNullOrEmpty(p.AddressArea) && p.Buro == buro
                        select new nulldata
                        {
                            ProtocolNumber = p.ProtocolNumber,
                            FIO = string.Format("{0} {1} {2}", p.LastName,
                                                               p.FirstName,
                                                               p.MiddleName),
                            Gender = p.Gender,
                            Age = p.Age,
                            AddressFull = p.AddressFull,
                            CodeMKB = p.CodeMKB,
                            DateDecision = p.DateDecision
                        }).ToList();

            // get sum of people
            var result = from item in query
                        group item by new
                        {
                            item.District,
                            item.Area,
                            item.CityType,
                            item.Exam,
                            item.Code
                        } into g
                        orderby g.Key.Area, g.Key.CityType, g.Key.Exam, g.Key.Code
                    select new GroupingData
                    {
                        District = g.Key.District,
                        Area = g.Key.Area,
                        CityType = g.Key.CityType,
                        Exam = g.Key.Exam,
                        Code = g.Key.Code,
                        Sum = g.Count()
                    };
            return result;
        }

        public string SetDistrict(string Value)
        {
            string[] districts = { "Авиастроительный", 
                                   "Вахитовский", 
                                   "Кировский", 
                                   "Московский", 
                                   "Ново-Савиновский", 
                                   "Приволжский", 
                                   "Советский"
                                 };
            string result = "";
            foreach (var elem in districts)
            {
                if (Value.StartsWith(elem))
                {
                    result = "Казань";
                    break;
                }
            }
            if (string.IsNullOrEmpty(result))
            {
                result = "РТ";
            }
            return result;
        }

        internal List<ReportData> GetReport(string buro)
        {
            var groupData = GroupMKB(buro);

            var temp = from item in groupData
                       select new ReportData
                         {
                             District = item.District,
                             Area = item.Area,
                             CityType = item.CityType,
                             Exam = item.Exam,
                             A1519 = (item.Code == "A15-19") ? item.Sum : 0,
                             C = (item.Code == "C") ? item.Sum : 0,
                             F = (item.Code == "F") ? item.Sum : 0,
                             H6095 = (item.Code == "H60-95") ? item.Sum : 0,
                             E = (item.Code == "E") ? item.Sum : 0,
                             G = (item.Code == "G") ? item.Sum : 0,
                             H0059 = (item.Code == "H00-59") ? item.Sum : 0,
                             I = (item.Code == "I") ? item.Sum : 0,
                             J = (item.Code == "J") ? item.Sum : 0,
                             K = (item.Code == "K") ? item.Sum : 0,
                             M = (item.Code == "M") ? item.Sum : 0,
                             ST = (item.Code == "ST") ? item.Sum : 0,
                             Q = (item.Code == "Q") ? item.Sum : 0,
                             P = (item.Code == "P") ? item.Sum : 0,
                             N = (item.Code == "N") ? item.Sum : 0,
                             Other = (item.Code == "OTHER") ? item.Sum : 0
                         };
            var result = from p in temp
                         group p by new
                         {
                             p.District,
                             p.Area,
                             p.CityType,
                             p.Exam
                         } into g
                         orderby g.Key.District descending, g.Key.Area, g.Key.CityType, g.Key.Exam
                         select new ReportData
                         {
                             District = g.Key.District,
                             Area = g.Key.Area,
                             CityType = g.Key.CityType,
                             Exam =g.Key.Exam,
                             A1519 = g.Sum(x => x.A1519),
                             C = g.Sum(x => x.C),
                             F = g.Sum(x => x.F),
                             H6095 = g.Sum(x => x.H6095),
                             E = g.Sum(x => x.E),
                             G = g.Sum(x =>x.G),
                             H0059 = g.Sum(x => x.H0059),
                             I = g.Sum(x => x.I),
                             J = g.Sum(x => x.J),
                             K = g.Sum(x => x.K),
                             M = g.Sum(x => x.M),
                             ST = g.Sum(x => x.ST),
                             Q = g.Sum(x => x.Q),
                             P = g.Sum(x => x.P),
                             N = g.Sum(x => x.N),
                             Other = g.Sum(x => x.Other)
                         };
            FinalReport = result.ToList();
            return FinalReport;
        }
        internal void SaveErrors(string buro)
        {
            var endname = "";
            switch (reportType)
            {
                case 1: endname = "дети";
                    break;
                case 2: endname = "взрослые(труд)";
                    break;
                case 3: endname = "взрослые(пенс)";
                    break;
            }
            reportName = Path.Combine(reportOutPath, string.Format("{0}_{1}_{2}_{3:yyyyMMdd_HHmmss}.csv", buro, endname, "Ошибки", DateTime.Now));
            var CsvExport = new CsvExport<nulldata>(nullData);
            CsvExport.ExportToFile(reportName);
        }

        internal void SaveFile(string buro)
        {
            var endname = "";
            switch (reportType)
            {
                case 1: endname = "дети";
                    break;
                case 2: endname = "взрослые(труд)";
                    break;
                case 3: endname = "взрослые(пенс)";
                    break;
            }
            reportName = Path.Combine(reportOutPath, string.Format("{0}_{1}_{2:yyyyMMdd_HHmmss}.xls", buro, endname, DateTime.Now));
            var rangeRT = from p in FinalReport
                          where p.District == "РТ"
                          select p;

            var rangeKZN = from p in FinalReport
                           where p.District == "Казань"
                           select p;

            var sumKZN = from p in rangeKZN
                         group p by new
                         {
                             p.District,
                             p.CityType,
                             p.Exam
                         } into g
                         orderby g.Key.District descending, g.Key.CityType, g.Key.Exam
                         select new ReportData
                         {
                             District = g.Key.District,
                             Area = "г.Казань",
                             CityType = g.Key.CityType,
                             Exam = g.Key.Exam,
                             A1519 = g.Sum(x => x.A1519),
                             C = g.Sum(x => x.C),
                             F = g.Sum(x => x.F),
                             H6095 = g.Sum(x => x.H6095),
                             E = g.Sum(x => x.E),
                             G = g.Sum(x => x.G),
                             H0059 = g.Sum(x => x.H0059),
                             I = g.Sum(x => x.I),
                             J = g.Sum(x => x.J),
                             K = g.Sum(x => x.K),
                             M = g.Sum(x => x.M),
                             ST = g.Sum(x => x.ST),
                             Q = g.Sum(x => x.Q),
                             P = g.Sum(x => x.P),
                             N = g.Sum(x => x.N),
                             Other = g.Sum(x => x.Other)
                         };
            Excel.Application app = new Excel.Application();
            //Excel.Workbook workBook = null;
            try
            {
                // app = new Excel.Application();
                app.Visible = false;
                var workBook = app.Workbooks.Open(reportTemplatePath);
                var sheetFirst = (Excel._Worksheet)workBook.Sheets[1];
                var sheetSecond = (Excel._Worksheet)workBook.Sheets[2];
                var sheetThird = (Excel._Worksheet)workBook.Sheets[3];
                // sheet.Activate();
                // Первично и Вторично
                for (var i = 4; i < 49; i++)
                {
                    var CurrentArea = (Excel.Range)sheetFirst.Cells[i, 2];
                    var data = (CurrentArea.Text == "г.Казань") ? sumKZN : rangeRT;

                    foreach (var item in data)
                    {
                        if (item.Area == CurrentArea.Text)
                        {
                            Excel._Worksheet activeSheet = null;
                            if (item.Exam == "первично")
                            {
                                activeSheet = sheetFirst;
                            }
                            if (item.Exam == "повторно")
                            {
                                activeSheet = sheetSecond;
                            }

                            if (activeSheet != null)
                            {
                                if (item.CityType == "Городское поселение")
                                {
                                    activeSheet.Cells[i, 5] = item.A1519;
                                    activeSheet.Cells[i, 7] = item.C;
                                    activeSheet.Cells[i, 9] = item.F;
                                    activeSheet.Cells[i, 11] = item.H6095;
                                    activeSheet.Cells[i, 13] = item.E;
                                    activeSheet.Cells[i, 15] = item.G;
                                    activeSheet.Cells[i, 17] = item.H0059;
                                    activeSheet.Cells[i, 19] = item.J;
                                    activeSheet.Cells[i, 21] = item.I;
                                    activeSheet.Cells[i, 23] = item.K;
                                    activeSheet.Cells[i, 25] = item.M;
                                    activeSheet.Cells[i, 27] = item.ST;
                                    activeSheet.Cells[i, 29] = item.Q;
                                    activeSheet.Cells[i, 31] = item.P;
                                    activeSheet.Cells[i, 33] = item.N;
                                    activeSheet.Cells[i, 35] = item.Other;
                                }
                                if (item.CityType == "Сельское поселение")
                                {
                                    activeSheet.Cells[i, 6] = item.A1519;
                                    activeSheet.Cells[i, 8] = item.C;
                                    activeSheet.Cells[i, 10] = item.F;
                                    activeSheet.Cells[i, 12] = item.H6095;
                                    activeSheet.Cells[i, 14] = item.E;
                                    activeSheet.Cells[i, 16] = item.G;
                                    activeSheet.Cells[i, 18] = item.H0059;
                                    activeSheet.Cells[i, 20] = item.J;
                                    activeSheet.Cells[i, 22] = item.I;
                                    activeSheet.Cells[i, 24] = item.K;
                                    activeSheet.Cells[i, 26] = item.M;
                                    activeSheet.Cells[i, 28] = item.ST;
                                    activeSheet.Cells[i, 30] = item.Q;
                                    activeSheet.Cells[i, 32] = item.P;
                                    activeSheet.Cells[i, 34] = item.N;
                                    activeSheet.Cells[i, 36] = item.Other;
                                }
                            }

                        }
                    }
                }
                //Казань
                for (var i = 6; i < 13; i++)
                {
                    var CurrentArea = (Excel.Range)sheetThird.Cells[i, 2];
                    foreach (var item in rangeKZN)
                    {
                        if (item.Area == CurrentArea.Text)
                        {
                            Excel._Worksheet activeSheet = sheetThird;
                            int j = -1;
                            if (item.Exam == "первично")
                            {
                                j = 0;
                            }
                            if (item.Exam == "повторно")
                            {
                                j = 12;
                            }

                            if (j != -1)
                            {
                                if (item.CityType == "Городское поселение")
                                {
                                    activeSheet.Cells[i + j, 4] = item.A1519;
                                    activeSheet.Cells[i + j, 5] = item.C;
                                    activeSheet.Cells[i + j, 6] = item.F;
                                    activeSheet.Cells[i + j, 7] = item.H6095;
                                    activeSheet.Cells[i + j, 8] = item.E;
                                    activeSheet.Cells[i + j, 9] = item.G;
                                    activeSheet.Cells[i + j, 10] = item.H0059;
                                    activeSheet.Cells[i + j, 11] = item.J;
                                    activeSheet.Cells[i + j, 12] = item.I;
                                    activeSheet.Cells[i + j, 13] = item.K;
                                    activeSheet.Cells[i + j, 14] = item.M;
                                    activeSheet.Cells[i + j, 15] = item.ST;
                                    activeSheet.Cells[i + j, 16] = item.Q;
                                    activeSheet.Cells[i + j, 17] = item.P;
                                    activeSheet.Cells[i + j, 18] = item.N;
                                    activeSheet.Cells[i + j, 19] = item.Other;
                                }
                            }

                        }
                    }
                }
                workBook.SaveAs(reportName);
                workBook.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                //workBook = null;
                app.Quit();
                try
                {
                    app = null;
                }
                catch (Exception) { }
                GC.Collect();
            }

        }
    }
    //nullData
    public class nulldata
    {
        public string ProtocolNumber { get; set; }
        public string FIO { get; set; }
        public string Gender { get; set; }
        public string Age { get; set; }
        public string AddressFull { get; set; }
        public string CodeMKB { get; set; }
        public string DateDecision { get; set; }
    }

    // Report
    public class ReportData
    {
        public string District { get; set; }
        public string Area {get ; set;} 
        public string CityType {get ; set;}
        public string Exam {get ; set;}
        public int A1519 {get ; set;}
        public int C { get; set; }
        public int F { get; set; }
        public int H6095 { get; set; }
        public int E { get; set; }
        public int G { get; set; }
        public int H0059 { get; set; }
        public int I { get; set; }
        public int J { get; set; }
        public int K { get; set; }
        public int M { get; set; }
        public int ST { get; set; }
        public int Q { get; set; }
        public int P { get; set; }
        public int N { get; set; }
        public int Other { get; set; }
    }
    // Patient
    public class Patient
    {
            public string	    ProtocolNumber		{get;set;}
            public string	    LastName			{get;set;}
            public string		FirstName			{get;set;}
            public string		MiddleName			{get;set;}
            public string		Gender				{get;set;}
            public string		Age					{get;set;}
            public string		AddressFull			{get;set;}
            public string		AddressCityArea		{get;set;}
            public string		AddressArea			{get;set;}
            public string		AddressCity			{get;set;}
            public string		Exam				{get;set;}
            public string		GroupDisability		{get;set;}
            public string		CodeMKB				{get;set;}
            public string		OrganisationName	{get;set;}
            public string		DateDecision		{get;set;}
            public string		TermDisability		{get;set;}
            public string		ObjectivesMSE		{get;set;}
            public string		DateBirth			{get;set;}
            public string       Buro                {get;set;}
    }
    // TempData
    public class GroupingData
    {
        public string District  { get; set; }
        public string Area      { get; set; }
        public string CityType  { get; set; }
        public string Exam      { get; set; }
        public string Code      { get; set; }
        public int Sum       { get; set; }
    }

    // Классы для лечения кривых ручек
    public class Mask
    {
        public List<Element> MaskList = new List<Element>();

        public string Equal(string target, TypeMask type)
        {
            var oldValue = target;
            foreach (var item in MaskList)
            {
                var result = false;
                switch (item.Type)
                {
                    case TypeEnum.Contains:
                        result = target.Contains(item.Value);
                        break;
                    case TypeEnum.Start:
                        result = target.StartsWith(item.Value);
                        break;
                    case TypeEnum.End:
                        result = target.EndsWith(item.Value);
                        break;
                    case TypeEnum.Between:
                        result = String.Compare(target, item.Value) > 0 && String.Compare(target, item.Value2) < 0;
                        break;
                }
                if (result)
                {
                    target = item.NewValue;
                    break;
                }
            }
            if (type == TypeMask.Code && oldValue == target) target = "OTHER";

            return target;
        }

        public List<Element> GenerateAreaMask()
        {
            var result = new List<Element>()
            {
                {new Element(){Type= TypeEnum.Start ,Value="авиастро",NewValue="Авиастроительный"}},
                {new Element(){Type= TypeEnum.Start ,Value="вахит",NewValue="Вахитовский"}},
                {new Element(){Type= TypeEnum.Start ,Value="киров",NewValue="Кировский"}},
                {new Element(){Type= TypeEnum.Start ,Value="москов",NewValue="Московский"}},
                {new Element(){Type= TypeEnum.Contains ,Value="савин",NewValue="Ново-Савиновский"}},
                {new Element(){Type= TypeEnum.Start ,Value="привол",NewValue="Приволжский"}},
                {new Element(){Type= TypeEnum.Start ,Value="совет",NewValue="Советский"}},
                {new Element(){Type= TypeEnum.Start ,Value="агрыз",NewValue="Агрызский"}},
                {new Element(){Type= TypeEnum.Start ,Value="азнак",NewValue="Азнакаевский"}},
                {new Element(){Type= TypeEnum.Start ,Value="аксу",NewValue="Аксубаевский"}},
                {new Element(){Type= TypeEnum.Start ,Value="акта",NewValue="Актанышский"}},
                {new Element(){Type= TypeEnum.Start ,Value="алекс",NewValue="Алексеевский"}},
                {new Element(){Type= TypeEnum.Start ,Value="альке",NewValue="Алькеевский"}},
                {new Element(){Type= TypeEnum.Start ,Value="альме",NewValue="Альметьевский"}},
                {new Element(){Type= TypeEnum.Start ,Value="апас",NewValue="Апастовский"}},
                {new Element(){Type= TypeEnum.Start ,Value="арс",NewValue="Арский"}},
                {new Element(){Type= TypeEnum.Start ,Value="атн",NewValue="Атнинский"}},
                {new Element(){Type= TypeEnum.Start ,Value="бавл",NewValue="Бавлинский"}},
                {new Element(){Type= TypeEnum.Start ,Value="балта",NewValue="Балтасинский"}},
                {new Element(){Type= TypeEnum.Start ,Value="бугул",NewValue="Бугульминский"}},
                {new Element(){Type= TypeEnum.Start ,Value="буин",NewValue="Буинский"}},
                {new Element(){Type= TypeEnum.Contains ,Value="услон",NewValue="В.Услонский"}},
                {new Element(){Type= TypeEnum.Start ,Value="высокогор",NewValue="Высокогорский"}},
                {new Element(){Type= TypeEnum.Start ,Value="дрож",NewValue="Дрожжановский"}},
                {new Element(){Type= TypeEnum.Start ,Value="елаб",NewValue="Елабужский"}},
                {new Element(){Type= TypeEnum.Start ,Value="заин",NewValue="Заинский"}},
                {new Element(){Type= TypeEnum.Start ,Value="зелен",NewValue="Зеленодольский"}},
                {new Element(){Type= TypeEnum.Contains ,Value="усть",NewValue="К.Устьинский"}},
                {new Element(){Type= TypeEnum.Start ,Value="кайб",NewValue="Кайбицкий"}},
                {new Element(){Type= TypeEnum.Start ,Value="кукм",NewValue="Кукморский"}},
                {new Element(){Type= TypeEnum.Start ,Value="лаиш",NewValue="Лаишевский"}},
                {new Element(){Type= TypeEnum.Start ,Value="ленин",NewValue="Лениногорский"}},
                {new Element(){Type= TypeEnum.Start ,Value="мамад",NewValue="Мамадышский"}},
                {new Element(){Type= TypeEnum.Start ,Value="менд",NewValue="Менделеевский"}},
                {new Element(){Type= TypeEnum.Start ,Value="менз",NewValue="Мензелинский"}},
                {new Element(){Type= TypeEnum.Start ,Value="мусл",NewValue="Муслюмовский"}},
                {new Element(){Type= TypeEnum.Start ,Value="нижнек",NewValue="Нижнекамский"}},
                {new Element(){Type= TypeEnum.Contains ,Value="шешм",NewValue="Н.Шешминский"}},
                {new Element(){Type= TypeEnum.Start ,Value="нурл",NewValue="Нурлатский"}},
                {new Element(){Type= TypeEnum.Start ,Value="пестр",NewValue="Пестречинский"}},
                {new Element(){Type= TypeEnum.Contains ,Value="слобо",NewValue="Рыбнослободский"}},
                {new Element(){Type= TypeEnum.Start ,Value="саби",NewValue="Сабинский"}},
                {new Element(){Type= TypeEnum.Start ,Value="сарм",NewValue="Сармановский"}},
                {new Element(){Type= TypeEnum.Start ,Value="спас",NewValue="Спасский"}},
                {new Element(){Type= TypeEnum.Start ,Value="тетю",NewValue="Тетюшский"}},
                {new Element(){Type= TypeEnum.Start ,Value="тука",NewValue="Тукаевский"}},
                {new Element(){Type= TypeEnum.Start ,Value="тюля",NewValue="Тюлячинский"}},
                {new Element(){Type= TypeEnum.Start ,Value="черем",NewValue="Черемшанский"}},
                {new Element(){Type= TypeEnum.Start ,Value="чисто",NewValue="Чистопольский"}},
                {new Element(){Type= TypeEnum.Start ,Value="юта",NewValue="Ютазинский"}}
            };
            return result;
        }

        public List<Element> GenerateCodeMask()
        {
            var result = new List<Element>()
            {
                {new Element(){Type= TypeEnum.Start ,Value="C",NewValue="C"}},
                {new Element(){Type= TypeEnum.Start ,Value="F",NewValue="F"}},
                {new Element(){Type= TypeEnum.Start ,Value="E",NewValue="E"}},
                {new Element(){Type= TypeEnum.Start ,Value="G",NewValue="G"}},
                {new Element(){Type= TypeEnum.Start ,Value="I",NewValue="J"}}, //*
                {new Element(){Type= TypeEnum.Start ,Value="J",NewValue="I"}}, //*
                {new Element(){Type= TypeEnum.Start ,Value="K",NewValue="K"}},
                {new Element(){Type= TypeEnum.Start ,Value="M",NewValue="M"}},
                {new Element(){Type= TypeEnum.Start ,Value="S",NewValue="ST"}},  //*
                {new Element(){Type= TypeEnum.Start ,Value="T",NewValue="ST"}},  //*
                {new Element(){Type= TypeEnum.Start ,Value="Q",NewValue="Q"}},
                {new Element(){Type= TypeEnum.Start ,Value="P",NewValue="P"}},
                {new Element(){Type= TypeEnum.Start ,Value="N",NewValue="N"}},
                {new Element(){Type= TypeEnum.Between ,Value="A15",Value2="A19",NewValue="A15-19"}},
                {new Element(){Type= TypeEnum.Between ,Value="H60",Value2="H95",NewValue="H60-95"}},
                {new Element(){Type= TypeEnum.Between ,Value="H00",Value2="H59",NewValue="H00-59"}}
            };
            return result;
        }
        public class Element
        {
            public string Value { get; set; }
            public string Value2 { get; set; }
            public TypeEnum Type { get; set; }
            public string NewValue { get; set; }
        }
        public enum TypeEnum
        {
            Contains, Start, End, Between
        }
        public enum TypeMask
        {
            Area,Code
        }
    }

    public class CsvExport<T> where T : class
    {
        public List<T> Objects;

        public CsvExport(List<T> objects)
        {
            Objects = objects;
        }

        public string Export()
        {
            return Export(true);
        }

        public string Export(bool includeHeaderLine)
        {
            var delimeter = ";";
            StringBuilder sb = new StringBuilder();
            //Get properties using reflection.
            IList<PropertyInfo> propertyInfos = typeof(T).GetProperties();

            if (includeHeaderLine)
            {
                //add header line.
                foreach (PropertyInfo propertyInfo in propertyInfos)
                {
                    sb.Append(propertyInfo.Name).Append(delimeter);
                }
                sb.Remove(sb.Length - 1, 1).AppendLine();
            }

            //add value for each property.
            foreach (T obj in Objects)
            {
                foreach (PropertyInfo propertyInfo in propertyInfos)
                {
                    sb.Append(MakeValueCsvFriendly(propertyInfo.GetValue(obj, null))).Append(delimeter);
                }
                sb.Remove(sb.Length - 1, 1).AppendLine();
            }

            return sb.ToString();
        }

        //export to a file.
        public void ExportToFile(string path)
        {
            File.WriteAllText(path, Export(), Encoding.GetEncoding(1251));
        }

        //export as binary data.
        public byte[] ExportToBytes()
        {
            return Encoding.UTF8.GetBytes(Export());
        }

        //get the csv value for field.
        private string MakeValueCsvFriendly(object value)
        {
            if (value == null) return "";
            if (value is Nullable && ((INullable)value).IsNull) return "";

            if (value is DateTime)
            {
                if (((DateTime)value).TimeOfDay.TotalSeconds == 0)
                    return ((DateTime)value).ToString("yyyy-MM-dd");
                return ((DateTime)value).ToString("yyyy-MM-dd HH:mm:ss");
            }
            string output = value.ToString();

            if (output.Contains(",") || output.Contains("\""))
                output = '"' + output.Replace("\"", "\"\"") + '"';

            return output;

        }
    }
  
}
