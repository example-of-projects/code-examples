'use strict'

angular.module('someApp.SomeModule')
  .directive 'simpleTable', [
    'BASE_PATH_SOMEMODULE'
    (BASE_PATH) ->
      restrict: 'E'
      templateUrl: BASE_PATH + 'views/directives/simple-table.html'
      scope:
        sort: '='
        query: '='
        header: '='
        total: '@'
        callback: '&'
      controller: [
        '$scope'
        ($scope) ->
          toggleDirection = ->
            $scope.sort.direction = if $scope.sort.direction is 'ASC' then 'DESC' else 'ASC'

          toggleOrder = (item) ->
            if $scope.sort.field_name is item.fieldName
              toggleDirection()
            else
              $scope.sort.field_name = item.fieldName
              $scope.sort.direction = 'ASC'
            $scope.callback().call(@)

          isSortedField = (item) ->
            $scope.sort.field_name is item.fieldName

          $scope.toggleOrder = toggleOrder
          $scope.isSortedField = isSortedField

          do ->
            toggleOrder(_.find($scope.header, {sorted: true}))
      ]
]
