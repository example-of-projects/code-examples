#  Эта фабрика служит для частичной-кастомной замены шаблонов в ui-select
#  Кастомные темплейты находятся внутри фабрики т.к. на момент написание это было тестовое решение
'use strict'

angular.module('someFactories')
  .factory 'TemplateStorage', [
    '$templateCache'
    ($templateCache) ->
      matchTpl = [
        '<div class="ui-select-match" ng-hide="$select.open" ',
        ' ng-disabled="$select.disabled" ',
        ' ng-class="{\'btn-default-focus\':$select.focus}">',
        '<span tabindex="-1" class="btn btn-default btn-journal ui-select-toggle" ',
        ' aria-label="{{ $select.baseTitle }} activate" ng-disabled="$select.disabled" ',
        ' ng-click="$select.activate()" style="outline: 0;">',
        '<span ng-show="$select.isEmpty()" class="ui-select-placeholder text-muted">',
        '{{$select.placeholder}}',
        '</span> ',
        '<span ng-hide="$select.isEmpty()" class="ui-select-match-text pull-left" ',
        ' ng-class="{\'ui-select-allow-clear\': $select.allowClear && !$select.isEmpty()}"',
        ' ng-transclude="">',
        '</span> ',
        '<i class="caret pull-right" ng-click="$select.toggle($event)"></i> ',
        '<a ng-show="$select.allowClear && !$select.isEmpty()" aria-label="{{ $select.baseTitle }} clear"',
        ' style="margin-right: 10px" ng-click="$select.clear($event)" class="btn btn-xs btn-link pull-right">',
        '<i class="glyphicon glyphicon-remove" aria-hidden="true"></i>',
        '</a>',
        '</span>',
        '</div>'
      ].join('')
      matchMultipleTpl = [
        '<span class="ui-select-match">',
        '<span ng-repeat="$item in $select.selected">',
        '<span class="ui-select-match-item btn btn-default btn-xs" tabindex="-1"',
        ' type="button" ng-disabled="$select.disabled"',
        ' ng-click="$selectMultiple.activeMatchIndex = $index;"',
        ' ng-class="{\'select-locked\':$select.isLocked(this, $index)}"',
        ' ng-style="{borderColor: $item.color}"',
        ' ui-select-sort="$select.selected">',
        '<span class="close ui-select-match-close"',
        ' ng-style="{color: $item.color}"',
        ' ng-hide="$select.disabled"',
        ' ng-click="$selectMultiple.removeChoice($index)">',
        '&nbsp;&times;',
        '</span>',
        '<span uis-transclude-append=\"\"></span>',
        '</span>',
        '</span>',
        '</span>'
      ].join('')

      # =======  Get методы для получения темплейтов
      # Выдает кастомные темплейты для ui-select которые используются на странице admin-journal
      getSomeModuleSelectTemplate = (templateName) ->
        switch templateName
          when 'match' then matchTpl
          when 'match-multiple' then matchMultipleTpl
      # Выдает кастомные темплейты для ui-select которые используются на странице statistics
      getSomeModule2SelectTemplate = (templateName) ->
        switch templateName
          when 'match' then matchTpl
          when 'match-multiple' then matchMultipleTpl
      # =======
      # =======  Set методы для добавления темплейтов templatecache
      setSomeModule2SelectTemplate = ->
        setSelectTemplate('journal', getSomeModuleSelectTemplate)

      setSomeModule2SelectTemplate = ->
        setSelectTemplate('statistic', getSomeModule2SelectTemplate)
      # =======
      # Set select template method
      setSelectTemplate = (page, getter) ->
        templates = [
          {from: 'bootstrap/choices.tpl.html',          to: '/choices.tpl.html',          type: 'main'}
          {from: 'bootstrap/select-multiple.tpl.html',  to: '/select-multiple.tpl.html',  type: 'main'}
          {from: 'bootstrap/select.tpl.html',           to: '/select.tpl.html',           type: 'main'}
          {from: 'match-multiple',                      to: '/match-multiple.tpl.html',   type: 'custom'}
          {from: 'match',                               to: '/match.tpl.html',            type: 'custom'}
        ]
        _.each templates, (template) ->
          switch template.type
            when 'main'
              $templateCache.put(page+template.to, $templateCache.get(template.from))
            when 'custom'
              $templateCache.put(page+template.to, getter(template.from))

      TemplateStorage = {
        getSomeModuleSelectTemplate:     getSomeModuleSelectTemplate
        getSomeModule2SelectTemplate:  getSomeModule2SelectTemplate
        setSomeModuleSelectTemplate:    setSomeModuleSelectTemplate
        setSomeModule2SelectTemplate:  setSomeModule2SelectTemplate
      }
]
