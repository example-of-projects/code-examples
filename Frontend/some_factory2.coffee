'use strict'

angular.module('someFactories')
.factory 'YearSettings', [
  '$resource'
  ($resource) ->
    year = $resource '/api_name/:id', { id: '@id' }, {
      save: { url: '/api_name/subname', method: 'POST' }
      update: { method: 'PUT' }
      readCopyResult: { method: 'PUT', url: '/api_name/copy_result' }
    }

    year::fullname = ->
      @name || "#{@id} - #{@id + 1} год"

    year::fullname_with_status = ->
      status = null
      switch @status
        when 'current'
          status = 'текущий'
        when 'future'
          status = 'новый'
        when 'archived'
          status = 'архивный'
      "#{@fullname()} (#{status})"

    year::datesRange = ->
      start_quarter_date =  moment(@quarter_1_start).format('DD.MM.YYYY')
      finish_quarter_date = moment(@quarter_4_end).format('DD.MM.YYYY')
      "#{start_quarter_date} - #{finish_quarter_date}"

    year::getStatus = ->
      {archived: 'Архивный', current: 'Текущий', future: 'Новый'}[@status]

    year::$saveOrUpdate = ->
      if @id
        @$update()
      else
        @$save()

    year
]
