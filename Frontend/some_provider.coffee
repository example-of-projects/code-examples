'use strict'

storageBuilder = (type) ->
  $get: [
    '$location'
    '$window'
    'localStorageService'
    ($location, $window, localStorageService) ->
      class StorageService
        constructor: ->
          @states = {}
          switch type
            when 'localStorage'
              @withSerialization = false
              @getStorage = localStorageService.get
              @setStorage = localStorageService.set
              @removeItemStorage = localStorageService.remove
              @removeAllStorage =   localStorageService.clearAll
            when 'sessionStorage'
              @withSerialization = true
              @getStorage = (key) -> $window.sessionStorage.getItem(key)
              @setStorage = (key, state) -> $window.sessionStorage.setItem(key, state)
              @removeItemStorage = (key) ->  $window.sessionStorage.removeItem(key)
              @removeAllStorage = -> $window.sessionStorage.clear()
          return

        set: (state, postfix, path = $location.path()) =>
          _state = if @withSerialization then angular.toJson(state) else state
          @setStorage @key(postfix, path), _state
          @states[@key(postfix, path)] = state

        get: (postfix, path = $location.path()) =>
          state = @getStorage @key(postfix, path)
          state = angular.fromJson state if @withSerialization
          @states[@key(postfix, path)] ?= state || {}

        getAndUnion: (defaults, postfix) =>
          if _.isObject(defaults)
            newFilters = _.defaults @get(postfix), defaults
            @set(newFilters, postfix)

        clear: =>
          @states = {}
          @removeAllStorage()

        remove: (postfix) =>
          key = @key(postfix)
          @removeItemStorage key
          @states[key] = {}

        key: (postfix, path = $location.path()) ->
          path += postfix if postfix
          path
      new StorageService()
  ]

angular.module('appProviders', [])
      .provider '$appLocalStorageProvider', storageBuilder('localStorage')
      .provider '$appSessionStorageProvider', storageBuilder('sessionStorage')
