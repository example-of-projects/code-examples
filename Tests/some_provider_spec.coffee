'use strict'

describe 'Provider: appLocalStorageProvider', ->
  beforeEach module('someApp')

  appLocalStorageProvider = null
  localStorageService = null
  states = {state1: '1', state2: '2'}
  defaults = {state2: '1', state3: '3'}

  beforeEach inject( ($appLocalStorageProvider, _localStorageService_) ->
    appLocalStorageProvider = $appLocalStorageProvider
    localStorageService = _localStorageService_
  )

  describe 'set', ->
    it 'set new state', ->
      appLocalStorageProvider.set(states)
      expect(appLocalStorageProvider.states['/']).toEqual(states)

    it 'set new state with postfix and path', ->
      appLocalStorageProvider.set(states, 'postfix', 'path')
      expect(appLocalStorageProvider.states['pathpostfix']).toEqual(states)

    it 'set new state with postfix', ->
      appLocalStorageProvider.set(states, 'postfix')
      expect(appLocalStorageProvider.states['/postfix']).toEqual(states)

    it 'returns state', ->
      new_state = appLocalStorageProvider.set(states, 'postfix')
      expect(new_state).toEqual(states)

  describe 'get', ->
    it 'get state', ->
      appLocalStorageProvider.set(states)
      expect(appLocalStorageProvider.get()).toEqual(states)

    it 'get state with postfix', ->
      appLocalStorageProvider.set(states, 'postfix')
      expect(appLocalStorageProvider.get('postfix')).toEqual(states)

    it 'get state with postfix and path', ->
      expect(appLocalStorageProvider.get('postfix', 'path')).toEqual(states)

  describe 'getAndUnion', ->
    beforeEach ->
      appLocalStorageProvider.set(states)

    it 'get and union defaults', ->
      expect(appLocalStorageProvider.getAndUnion(defaults)).toEqual({state1: '1', state2: '2', state3: '3'})

    it 'get and union with return undefined if not object defaults', ->
      expect(appLocalStorageProvider.getAndUnion('')).toEqual(undefined)

  describe 'key', ->
    it 'return slash without postfix', ->
      expect(appLocalStorageProvider.key()).toEqual('/')

    it 'return slash with postfix', ->
      expect(appLocalStorageProvider.key('postifx')).toEqual('/postifx')

  describe 'states', ->
    beforeEach ->
      appLocalStorageProvider.set(states)
      appLocalStorageProvider.set(states, 'postfix')

    it 'states be 2 elements', ->
      expect(_.keys(appLocalStorageProvider.states).length).toEqual(2)

    it 'states be 2 elements after set another state', ->
      appLocalStorageProvider.set(defaults, 'postfix')
      appLocalStorageProvider.set(defaults)
      expect(_.keys(appLocalStorageProvider.states).length).toEqual(2)

  describe 'remove', ->
    beforeEach ->
      appLocalStorageProvider.set(states, 'postfix')

    it 'removes object from states', ->
      expect(appLocalStorageProvider.states['/postfix']).toEqual states
      appLocalStorageProvider.remove('postfix')
      expect(appLocalStorageProvider.states['/postfix']).toEqual {}

    it 'removes object from localStorageService', ->
      expect(localStorageService.get('/postfix')).toEqual states
      appLocalStorageProvider.remove('postfix')
      expect(localStorageService.get('/postfix')).toBeNull()
