'use strict'

describe 'Service: RedirectService', ->
  beforeEach module('egeApp')
  goFake = (state, params) ->
    state
  beforeEach inject (_$state_, _$cookies_, _$location_,
    RedirectService, HOST, SomeService, localStorageService) ->
    @$state = _$state_
    @$cookies = _$cookies_
    @$location = _$location_
    @RedirectService = RedirectService
    @HOST = HOST
    @SomeService = SomeService
    @localStorageService = localStorageService

  describe 'method calling', ->

    it 'redirectAfterAuth has been called', ->
      spyOn @RedirectService, 'redirectAfterAuth'
      @RedirectService.redirectAfterAuth()
      expect(@RedirectService.redirectAfterAuth).toHaveBeenCalled()

    it 'stateGo has been called', ->
      spyOn @$state, 'go'
      @RedirectService.redirectAfterAuth()
      expect(@$state.go).toHaveBeenCalled()

  describe 'localStorageService ', ->

    it 'remove redirectAfterLoginUrl', ->
      @localStorageService.cookie.set 'redirectAfterLoginUrl', 'foo'
      @RedirectService.redirectAfterAuth()
      expect(@localStorageService.cookie.get('redirectAfterLoginUrl')).toBe null

    it 'remove moduleAfterLogin', ->
      @localStorageService.cookie.set 'moduleAfterLogin', 'foo'
      @RedirectService.redirectAfterAuth()
      expect(@localStorageService.cookie.get('moduleAfterLogin')).toBe null

    it 'remove redirectToPay', ->
      @localStorageService.cookie.set 'redirectToPay', 'foo'
      @RedirectService.redirectAfterAuth()
      expect(@localStorageService.cookie.get('redirectToPay')).toBe null

    it 'remove redirectBackToModule', ->
      @$cookies.put 'redirectBackToModule', 'foo'
      @RedirectService.redirectAfterAuth()
      expect(@$cookies.get('redirectBackToModule')).toBe undefined

  describe 'user roles switch', ->
    beforeEach ->
      spyOn(@$state, 'go').and.callFake goFake

    describe 'when admin', ->

      it 'go to some.state', ->
        spyOn(@SomeService, 'getRole').and.returnValue 'admin'
        @RedirectService.redirectAfterAuth()
        expect(@$state.go).toHaveBeenCalledWith 'some.state'

    describe 'when user', ->

      it 'go to state1', ->
        @localStorageService.cookie.set 'redirectToPay', 'true'
        spyOn(@SomeService, 'getRole').and.returnValue 'user'
        @RedirectService.redirectAfterAuth()
        expect(@$state.go).toHaveBeenCalledWith 'state1'

      it 'go to state2', ->
        @localStorageService.cookie.set 'moduleAfterLogin', '1'
        spyOn(@SomeService, 'getRole').and.returnValue 'user'
        @RedirectService.redirectAfterAuth()
        expect(@$state.go).toHaveBeenCalledWith 'state2', {id: 1}

      it 'go to state3', ->
        @localStorageService.cookie.set 'redirectAfterLoginUrl', 'foo'
        spyOn(@SomeService, 'getRole').and.returnValue 'user'
        spyOn(@$location, 'path').and.callFake goFake
        @RedirectService.redirectAfterAuth()
        expect(@$location.path).toHaveBeenCalledWith 'foo'

      it 'go to state when params null', ->
        spyOn(@SomeService, 'getRole').and.returnValue 'user'
        @RedirectService.redirectAfterAuth()
        expect(@$state.go).toHaveBeenCalledWith 'state'
